var express = require('express');
var app = express();
var TSPSolver = require('./tspsolver');

app.get('/routes', function(req, res) {
	var solver = new TSPSolver();
	res.send('hi');
});

var port = 3000;
app.listen(port, function() {
	console.log('started on port: ' + port);
});