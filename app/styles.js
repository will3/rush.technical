import React, { StyleSheet } from 'react-native';
import colors from './colors';

const softShadowOpacity = 0.1;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%'
  },
  mapView: {
    position:'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%'
  },
  searchBarContainer: {
    position: 'absolute',
    left: 12,
    right: 12,
    top: 28,
    height: 50,
    shadowColor: '#000000',
    shadowOpacity: softShadowOpacity,
    shadowRadius: 4
  },
  searchBarBorder: {
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'row'
  },
  searchBarImage: {
    width: 32,
    height: 32,
  },
  searchBar: {
    flex: 1,
    height: 50,
    paddingLeft: 12
  },
  searchBarPlaceholder: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 10
  },
  searchBarPlaceholderText: {
    paddingTop: 2,
    color: colors.accent,
    fontSize: 16
  },
  destinationsView: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#FFFFFF',
    position: 'absolute',
    display: 'flex',
    shadowColor: '#000000',
    shadowOpacity: softShadowOpacity,
    shadowRadius: 4
  },
  toolbar: {
    height: 44,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  directionsText: {
    flex: 1,
    paddingLeft: 12,
    fontSize: 20,
    color: colors.accent
  },
  directionsButton: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: colors.accent,
    borderRadius: 4,
    overflow: 'hidden',
    marginRight: 12,
    height: 30,
    alignItems: 'center',
    paddingHorizontal: 8
  },
  directionsButtonImage: {
    width: 20,
    height: 20,
    marginRight: 4
  },
  directionsButtonText: {
    color: '#FFFFFF'
  },
  locationButton: {
    width: 44, 
    height: 44,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  locationButtonImage: {
    width: 32,
    height: 32
  },
  searchView: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    top: 90,
    right: 0,
    backgroundColor: '#FFFFFF'
  },
  searchRow: {
    display: 'flex',
    flexDirection: 'row',
    height: 60,
    alignItems: 'center',
    position: 'relative',
    width: '100%',
    paddingHorizontal: 12
  },
  searchAddImage: {
    width: 24,
    height: 24
  },
  searchRowTitle: {
    fontSize: 16,
    color: colors.accent
  },
  searchRowSubtitle: {
    fontSize: 12,
    color: colors.secondary
  },
  searchAddButton: {
    width: 44,
    height: 44,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 12
  },
  searchRowTexts: {
    flex: 1
  },
  divider: {
    height: 0.5, 
    backgroundColor: '#BBBBBB',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 99
  },
  correctingLocationBar: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 60,
    backgroundColor: '#FFFFFF',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  correctingLocationBarButton: {
    height: 44,
    backgroundColor: colors.accent,
    marginHorizontal: 12,
    paddingHorizontal: 12,
    borderRadius: 22,
    overflow: 'hidden',
    flex: 1
  },
  correctingLocationBarText: {
    color: 'white',
    lineHeight: 44,
    textAlign: 'center'
  },
  correctingLocationMessageText: {
    color: 'white',
    fontSize: 16
  },
  correctingLocationMessage: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    position: 'absolute',
    left: 12,
    top: 12,
    right: 12,
    padding: 12,
    borderRadius: 4,
    overflow: 'hidden'
  },
  clearButton: {
    paddingHorizontal: 12,
    height: 44
  },
  clearButtonText: {
    fontSize: 16,
    color: colors.accent,
    lineHeight: 44
  },
  expandingSpace: {
    flex: 1
  },
  placeholder: {
    fontSize: 16,
    color: colors.secondary,
    padding: 24,
    textAlign: 'center'
  },
  searchCancelButton: {
    height: 50
  },
  searchCancelButtonText: {
    fontSize: 16,
    color: colors.secondary,
    lineHeight: 50,
    paddingHorizontal: 12
  }
});

export default styles;