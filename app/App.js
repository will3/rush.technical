import uuidv4 from 'uuid/v4';
import React from 'react';
import { Dimensions, Button, TextInput, StyleSheet, Text, View, ScrollView, 
  Image, TouchableWithoutFeedback, Alert, TouchableOpacity, ActivityIndicator, TouchableHighlight, AsyncStorage } from 'react-native';
import MapView, { Marker, Polyline, Polygon } from 'react-native-maps';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import settings from './settings';
import api from './api';
import colors from './colors';
import Swipeout from 'react-native-swipeout';
import addIcon from './assets/add.png';
import styles from './styles';
import solveRoundTrip from './tsp';
import _ from 'lodash';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import notifications from './notifications';

momentDurationFormatSetup(moment);

function formatAddressFirstLine(place) {
  return place.formatted_address || '';
}

function formatAddressSecondLine(place) {
  const component = _.find(place.address_components, (component) => {
    return _.includes(component.types, 'sublocality');
  });

  if (component == null) {
    return '';
  }

  return component.long_name || '';
}

function handleError(err) {
  if (settings.showErrorVerbose) {
    Alert.alert(err.message, err.toString());
  } else {
    Alert.alert(err.message);
  }
}

function handleWarning(err) {
  console.warn(err.toString());
}

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locations: [],
      searching: false,
      predictions: [],
      searchBarText: '',
      places: [],
      correctingLocation: false,
      region: null,
      mapViewPixelDensity: 0,
      directions: null,
      legs: [],
      sortingPlaces: false
    };

    this.directionsPressed = this.directionsPressed.bind(this);
    this.onPlaceholderClicked = this.onPlaceholderClicked.bind(this);
    this.onSearchBarTextChanged = this.onSearchBarTextChanged.bind(this);
    this.session_token = uuidv4();
    this.autocompletePlaceThrottled = _.debounce(this.autocompletePlace, 200, { leading: false, trailing: true });
    this.onRemovePlace = this.onRemovePlace.bind(this);
    this.locationPressed = this.locationPressed.bind(this);
    this.onRegionChange = this.onRegionChange.bind(this);
    this.onLocationConfirm = this.onLocationConfirm.bind(this);
    this.onLocationCancel = this.onLocationCancel.bind(this);
    this.clearPressed = this.clearPressed.bind(this);

    this.onRegionChangeThrottled = _.debounce(this.onRegionChange, 400, { leading: false, trailing: true });

    this.lastLocation = null;
  }

  savePlaces(places) {
    const state = { places };
    AsyncStorage.setItem('places', JSON.stringify(state)).catch((err) => {
      handleWarning(err);
    });
  }

  saveLegs(legs) {
    const state = { legs };
    AsyncStorage.setItem('legs', JSON.stringify(state)).catch((err) => {
      handleWarning(err);
    });
  }

  async loadPlaces() {
    const state = JSON.parse(await AsyncStorage.getItem('places') || '{}');
    this.setState({ places: state.places || [] });
  }

  async loadLegs() {
    const state = JSON.parse(await AsyncStorage.getItem('legs') || '{}');
    this.setState({ legs: state.legs || [] }); 
  }

  onRegionChange(region) {
    if (this.state.correctingLocation) {
      this.setState({
        currentLocation: { longitude: region.longitude, latitude: region.latitude },
        region,
        legs: []
      });
    }

    this.setState({
      region,
      latitudeDelta: region.latitudeDelta   
    });
  }

  getBoundingRegion(places, currentLocation) {
    if (places.length === 0) {
      if (currentLocation != null) {
        return {
          longitude: currentLocation.longitude,
          latitude: currentLocation.latitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        };  
      } else {
        return {
          longitude: 174.7633,
          latitude: -36.8485,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        };
      }
    }

    var minLng = Infinity;
    var minLat = Infinity;
    var maxLng = -Infinity;
    var maxLat = -Infinity;
      
    const coords = places.map((place) => {
      return { 
        longitude: place.geometry.location.lng, 
        latitude: place.geometry.location.lat };
    });

    if (currentLocation != null) {
      coords.push(currentLocation);
    }

    for (var i = 0; i < coords.length; i++) {
      const coord = coords[i];
      if (coord.longitude < minLng) {
        minLng = coord.longitude;
      }
      if (coord.latitude < minLat) {
        minLat = coord.latitude;
      }
      if (coord.longitude > maxLng) {
        maxLng = coord.longitude;
      }
      if (coord.latitude > maxLat) {
        maxLat = coord.latitude;
      }
    }

    const diagonal = Math.sqrt((maxLng - minLng) * (maxLng - minLng) + (maxLat - minLat) * (maxLat - minLat)) * 2.0;

    const region = {
      longitude: (maxLng + minLng) / 2,
      latitude: (maxLat + minLat) / 2,
      longitudeDelta: diagonal,
      latitudeDelta: diagonal
    };

    region.latitude += region.latitudeDelta * 0.05;

    return region;
  }

  async autocompletePlace(text) {
    const response = await api.placesAutocomplete(text, this.session_token);
    if (!response.ok) {
      throw new Error('Something went wrong');
    }
    
    const json = await response.json();
    this.setState({
      predictions: json.predictions
    });
    return json;
  }

  onRemovePlace(place) {
    const places = this.state.places;
    _.pull(places, place);
    this.setState({ places, legs: [] });
    this.updateBoundingRegion(places, null);

    this.savePlaces(places);
  }

  onAddPlacePressed(prediction) {
    api.getPlace(prediction.place_id).then((response) => {
      if (response.ok) {
        return response.json();
      }

      throw new Error('Something went wrong');
    }).then((data) => {
      const existing = _.find(this.state.places, (place) => {
        return place.id === data.result.id;
      });
      if (existing != null) {
        Alert.alert('You\'ve added this location already');
        return;
      }

      const places = this.state.places;
      places.push(data.result);

      this.updateBoundingRegion(places, null);

      this.setState({ searching: false, places, searchBarText: '', legs: [], predictions: [] });
      this.refs.searchBar.blur();

      this.savePlaces(places);
      notifications.notifyAddress5minutely(data.result.formatted_address);
    }).catch((err) => {
      handleError(err);
    });
  }

  directionsPressed() {
    Alert.alert('Optimize your destinations?', 'This will sort your destinations in the best travel order',
    [
      {text: 'Cancel' },
      {text: 'OK', onPress: () => this.sortPlaces() },
    ],);
  }

  sortPlaces() {
    this.setState({
      sortingPlaces: true
    });
    solveRoundTrip({ 
      places: this.state.places, 
      currentLocation: this.state.currentLocation
    }, (tsp) => {
      const orders = tsp.getOrder();
      const places = this.state.places;
      for (var i = 1; i < orders.length; i++) {
        const placeIndex = orders[i] - 1;
        if (placeIndex === -1) {
          continue;
        }
        places[placeIndex].order = i;
      }

      const legs = tsp.getGDirections().routes[0].legs;
      this.saveLegs(legs);

      let totalTime = 0;
      for (var i = 1; i < legs.length; i++) {
        const leg = legs[i];
        totalTime += leg.duration.value;
        const time = moment.duration(totalTime, 'seconds').format('H[hrs]m[mins]');
        const order = i;
        this.state.places[orders[i] - 1].time = time;
      }

      this.setState({
        legs,
        sortingPlaces: false,
        places
      });
      this.savePlaces(places);
    }, (data) => {
      if (data != null) {
        const legs = data.routes[0].legs;
        this.setState({
          legs
        });
      }
    }, (err) => {
      handleError(err);
      this.setState({
        sortingPlaces: false,
        legs: []
      });
    });
  }

  onPlaceholderClicked() {
    this.setState({
      searching: true
    });

    this.refs.searchBar.focus();
  }

  onSearchBarTextChanged(text) {
    this.setState({
      searchBarText: text
    });

    this.autocompletePlaceThrottled(text);
  }

  componentDidMount() {
    navigator.geolocation.requestAuthorization();
    navigator.geolocation.getCurrentPosition((result) => {
      const currentLocation = result.coords;

      this.updateBoundingRegion(null, currentLocation);

      this.setState({
        currentLocation
      });

    }, (err) => {
      this.updateBoundingRegion(null, null);
      Alert.alert('Cannot get current location. Try adding your location to destinations.');
    });

    this.loadPlaces().then(() => {
      return this.loadLegs();
    }).catch((err) => {
      handleError(err);
    });
  }

  updateBoundingRegion(places, currentLocation) {
    places = places || this.state.places;
    currentLocation = currentLocation || this.state.currentLocation;
    const region = this.getBoundingRegion(places, currentLocation);
    this.setState({ region });
  }

  locationPressed() {
    this.lastLocation = this.state.currentLocation;

    this.setState({
      correctingLocation: true,
      region: {
        longitude: this.state.currentLocation.longitude,
        latitude: this.state.currentLocation.latitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      }
    });
  }

  onLocationConfirm() {
    this.setState({
      correctingLocation: false
    });
  }

  onLocationCancel() {
    const delta = {
      correctingLocation: false
    };
    if (this.lastLocation != null) {
      delta.currentLocation = this.lastLocation;
    }

    this.setState(delta);
  }

  clear() {
    this.setState({
      places: [],
      legs: []
    });
    this.savePlaces([]);
    this.saveLegs([]);
  }

  clearPressed() {
    if (this.state.places.length === 0) {
      return;
    }
    Alert.alert('Are you sure to remove all destinations?', '', [
      {text: 'Cancel' },
      {text: 'OK', onPress: this.clear.bind(this)},
    ])
  }

  render() {
    var { height, width } = Dimensions.get('window');
    const items = [];

    const mapViewBottomSearchBarCorrection = this.state.correctingLocation ? 60 : - 60;

    const placeholder = this.state.searching || this.state.searchBarText.length > 0 ? null : (
      <TouchableWithoutFeedback onPress={this.onPlaceholderClicked}>
        <View style={styles.searchBarPlaceholder}>
          <Image style={styles.searchBarImage} source={require('./assets/search.png')} />
          <Text style={styles.searchBarPlaceholderText}>Search here</Text>
        </View>
      </TouchableWithoutFeedback>
    );
    const destinationsViewHeight = this.state.correctingLocation ? 0 : 260;
    const mapViewHeight = height - destinationsViewHeight;

    const predictions = this.state.predictions.map((prediction, index) => {
      return (
        <TouchableHighlight key={prediction.id} onPress={this.onAddPlacePressed.bind(this, prediction)} 
          underlayColor={colors.selectedColor}>
          <View style={styles.searchRow}>
            <View style={styles.searchAddButton}>
              <Image style={styles.searchAddImage} source={addIcon}/>
            </View>
            <View style={styles.searchRowTexts}>
              <Text style={styles.searchRowTitle} numberOfLines={1}>{prediction.structured_formatting.main_text}</Text>
              <Text style={styles.searchRowSubtitle} numberOfLines={1}>{prediction.structured_formatting.secondary_text}</Text>
            </View>
            <View style={styles.divider}></View>
          </View>    
        </TouchableHighlight>
      );
    });
    const predictionsPlaceholderText = 'Enter address or place to add a destination';
    const predictionsPlaceholder = this.state.predictions.length === 0 ? (
      <Text style={styles.placeholder}>{predictionsPlaceholderText}</Text>
    ) : null;
    const searchView = this.state.searching ? (
      <View style={styles.searchView}>
        <KeyboardAwareScrollView style={{ width: '100%', height: '100%' }} 
        keyboardShouldPersistTaps='always'>
          {predictionsPlaceholder}
          {predictions}
          }
        </KeyboardAwareScrollView>
      </View> ) : null;

    const sortedPlaces = this.state.places.sort(function(a, b) {
      if (a.order == null || b.order == null) {
        return 0;
      }
      return a.order - b.order;
    });

    const places = sortedPlaces.map((place, index) => {
      const buttons = [{
        text: 'Remove',
        onPress: () => {
          this.onRemovePlace(place);
        },
        backgroundColor: '#ff3c13'
      }];

      // const orderText = place.order == null ? '' : `(${place.order}) `;
      const orderText = place.time == null ? '' : `(${place.time}) `;
      const title = `${orderText}${formatAddressFirstLine(place)}`;

      return (
        <View key={place.id || index}>
          <Swipeout backgroundColor='#FFFFFF' right={buttons}>
            <View style={styles.searchRow}>
              <View style={styles.searchRowTexts}>
              <Text style={styles.searchRowTitle} numberOfLines={1}>{title}</Text>
              <Text style={styles.searchRowSubtitle} numberOfLines={1}>{formatAddressSecondLine(place)}</Text>
              </View>
            </View>
          </Swipeout>
          <View style={styles.divider}></View>
        </View>
      );
    });

    const currentLocation = this.state.currentLocation;

    const markers = this.state.places.map((place, index) => {
      const marker = (<Marker 
        key={index} 
        coordinate={{ latitude: place.geometry.location.lat, longitude: place.geometry.location.lng }} 
        title={formatAddressFirstLine(place)}/>);
      return marker;
    });

    if (currentLocation != null) {
      const coord = { latitude: currentLocation.latitude, longitude: currentLocation.longitude }; 
      markers.push(<Marker pinColor={colors.accent} key='me' coordinate={coord} title='This is you'/>);
    }

    const searchBarCancelButton = this.state.searching ? (
      <TouchableOpacity style={styles.searchCancelButton} onPress={() => {
        this.setState({
          searching: false,
          searchBarText: '', 
          legs: [], 
          predictions: []
        });
        this.refs.searchBar.blur();
      }}>
        <Text style={styles.searchCancelButtonText}>Cancel</Text>
      </TouchableOpacity>
    ) : null;

    const searchBar = this.state.correctingLocation ? null : (
      <View style={styles.searchBarContainer}>
        <View style={styles.searchBarBorder}>
          <TextInput ref='searchBar' style={styles.searchBar} selectionColor={colors.accent}
          onChangeText={this.onSearchBarTextChanged} value={this.state.searchBarText} blurOnSubmit={false} 
          onBlur={() => {
            this.setState({ searching: false });
          }}
          onFocus={() => {
            this.setState({ searching: true });
          }}/>
          {searchBarCancelButton}
          {placeholder}
        </View>
      </View>);

    const destinationsButton = this.state.sortingPlaces ? (
      <View style={styles.directionsButton}>
        <ActivityIndicator size="small" color="#FFFFFF"/>
      </View>
    ) : (
      <TouchableOpacity style={styles.directionsButton} onPress={this.directionsPressed}>
        <Image style={styles.directionsButtonImage} source={require('./assets/directions.png')}></Image>
        <Text style={styles.directionsButtonText}>Directions</Text>
      </TouchableOpacity>
    );

    const clearButtonTextColor = this.state.places.length === 0 ? colors.secondary : colors.accent;

    const destinationsPlaceholder = this.state.places.length === 0 ? (
      <Text style={styles.placeholder}>Start by adding a destination</Text>
    ) : null;

    const destinationsView = this.state.correctingLocation ? null : (
      <View style={[styles.destinationsView, { height: destinationsViewHeight }]}>
        <View style={styles.toolbar}>
          <TouchableOpacity style={styles.clearButton} onPress={this.clearPressed}>
            <Text style={[styles.clearButtonText, { color: clearButtonTextColor }]}>Clear</Text>
          </TouchableOpacity>
          <View style={styles.expandingSpace}/>
          {destinationsButton}
          <View style={styles.divider}></View>
        </View>
        <ScrollView style={{ width: '100%', height: '100%', flex: 1 }}>
          {destinationsPlaceholder}
          {places}
          }
        </ScrollView>
      </View>
    );

    
    const region = this.state.region;

    const correctingLocationBar = this.state.correctingLocation ? (
      <View style={styles.correctingLocationBar}>
        <TouchableOpacity style={styles.correctingLocationBarButton} onPress={this.onLocationCancel}>
          <Text style={styles.correctingLocationBarText}>Cancel</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.correctingLocationBarButton} onPress={this.onLocationConfirm}>
          <Text style={styles.correctingLocationBarText}>Confirm</Text>
        </TouchableOpacity>
      </View>
    ) : null;

    const mapViewStyle = { position: 'absolute', left: 0, right: 0, top: 0, bottom: mapViewBottomSearchBarCorrection + destinationsViewHeight };

    const coordinates = [];

    var arrows = [];

    const strokeWidth = this.state.sortingPlaces ? 1 : 2;

    function rotateVector2(v, angle) {
      const cos = Math.cos(angle);
      const sin = Math.sin(angle);
      const tx = v[0];
      const ty = v[1];

      return [
        (cos * tx) - (sin * ty),
        (sin * tx) + (cos * ty)
      ];
    }

    function disVector2(v1, v2) {
      var dx = v1[0] - v2[0];
      var dy = v1[1] - v2[1];
      return Math.sqrt(dx * dx + dy * dy);
    }

    let totalTime = 0;
    const arrowSize = (this.state.latitudeDelta || 0.0922) * 0.05;
    const minDisBetweenArrows = arrowSize * 2;
    const minLengthForArrows = arrowSize / 2;
    const arrowCoords = [];

    function anyArrowsTooClose(v) {
      for (var i = 0; i < arrowCoords.length; i++) {
        const arrow = arrowCoords[i];
        const dis = disVector2(arrow, v);
        if (dis < minDisBetweenArrows) {
          return true;
        }
      }
      return false;
    }

    for (var i = 0; i < this.state.legs.length; i++) {
      const leg = this.state.legs[i];
      totalTime += leg.duration.value;
      for (var j = 0; j < leg.steps.length; j++) {
        const step = leg.steps[j];
        const longitude = step.start_location.lng;
        const latitude = step.start_location.lat;
        coordinates.push({ longitude, latitude });

        const midLng = (step.start_location.lng + step.end_location.lng) / 2;
        const midLat = (step.start_location.lat + step.end_location.lat) / 2;

        const dis = disVector2([ step.end_location.lng, step.end_location.lat ], 
          [ step.start_location.lng, step.start_location.lat ]);

        if (dis < minLengthForArrows) {
          continue;
        }

        const v = [ midLng, midLat ];
        if (anyArrowsTooClose(v)) {
          continue;
        }

        const latLngRatio = Math.sin(midLat);

        const dx = step.end_location.lng - step.start_location.lng;
        const dy = step.end_location.lat - step.start_location.lat;
        const angle = Math.atan2(dy / latLngRatio, dx);
        
        const vector = [ arrowSize, 0 ];

        const v1 = rotateVector2(vector, angle + Math.PI - 0.3);
        const v2 = rotateVector2(vector, angle + Math.PI + 0.3);

        arrowCoords.push([ midLng, midLat ]);

        const arrowCoordinates = [{
          longitude: midLng + v1[0],
          latitude: midLat + v1[1] * latLngRatio
        }, {
          longitude: midLng, latitude: midLat
        }, {
          longitude: midLng + v2[0],
          latitude: midLat + v2[1] * latLngRatio
        }];
        arrows.push(
          <Polygon key={arrows.length} coordinates={arrowCoordinates} fillColor={colors.accent}/>
        );

      }

      const time = moment.duration(totalTime, 'seconds').format('H[hrs]m[mins]');
    }

    if (coordinates.length > 0) {
      coordinates.push(coordinates[0]);
    }

    const pathOverlay = this.state.legs.length === 0 ? null : (
      <Polyline coordinates={coordinates} strokeColor={colors.accent} strokeWidth={strokeWidth}>
      </Polyline>
    );

    const dev = __DEV__ ? 
      (<Text style={{ position:'absolute', left: 0, bottom: destinationsViewHeight, padding: 8 }}>dev</Text>) : null;
    const mapView = region == null ? null : (
      <View style={[ styles.mapView ]}>
        <MapView style={mapViewStyle}
          region={region}
          onRegionChange={this.onRegionChangeThrottled}
        >
          {markers}
          {pathOverlay}
          {arrows}
        </MapView>
        <TouchableOpacity style={{ position:'absolute', right: 0, bottom: destinationsViewHeight }} onPress={this.locationPressed}>
          <View style={styles.locationButton}>
            <Image style={styles.locationButtonImage} source={require('./assets/location.png')}/>
          </View>
        </TouchableOpacity>
        {dev}
      </View>
    );

    const correctingLocationMessage = this.state.correctingLocation ? (
      <View style={styles.correctingLocationMessage}>
        <Text style={styles.correctingLocationMessageText}>
          Drag the map to correct your current location
        </Text>
      </View>
    ) : null;

    return (
      <View style={styles.container}>

        {mapView}

        {destinationsView}
        
        {searchBar}

        {searchView}

        {correctingLocationBar}

        {correctingLocationMessage}

      </View>
    );
  }
}