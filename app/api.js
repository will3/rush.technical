import querystring from 'querystring';
import settings from './settings';

function placesAutocomplete(input, session_token) {
	const params = {
		key: settings.gmKey,
		input,
		session_token,
		components: 'country:nz'
	};
	const query = querystring.encode(params);
	const url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?${query}`;
	return fetch(url);
}

function getDirections(params) {
	params.waypoints = formatLatLngArray(params.waypoints);
	params.origin = formatLatLng(params.origin);
	params.destination = formatLatLng(params.destination);
	params.key = settings.gmKey;
	params.path = 'enc:THE_ENCODED_POLYLINE';

	const query = querystring.encode(params);
	const url = `https://maps.googleapis.com/maps/api/directions/json?${query}`;
	return fetch(url);
}

function getPlace(placeid) {
	const params = {
		placeid,
		key: settings.gmKey
	};
	const query = querystring.encode(params);
	const url = `https://maps.googleapis.com/maps/api/place/details/json?${query}`;
	return fetch(url);
}

function formatLatLng(coord) {
	return coord.latitude + ',' + coord.longitude;
}

function formatLatLngArray(coords) {
	return coords.map((coord) => {
		return formatLatLng(coord);
	}).join('|');
}

const api = {
	placesAutocomplete, getDirections, getPlace
};

export default api;