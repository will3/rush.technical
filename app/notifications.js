import { PushNotificationIOS, Platform, AppState, Alert } from 'react-native';
import moment from 'moment';

function notifyAddress5minutely(address) {
	const seconds = 5 * 60 * 60;
	const scheduledDate = moment().add(seconds, 'seconds');
	if (Platform.OS === 'ios') {
		PushNotificationIOS.requestPermissions();
		PushNotificationIOS.cancelAllLocalNotifications();
		PushNotificationIOS.scheduleLocalNotification({
			fireDate: scheduledDate.format("YYYY-MM-DDTHH:mm:ss.sssZ"),
			alertTitle: '',
			alertBody: `${address}\nAdded at ${scheduledDate.format('/MMMM Do YYYY, h:mm:ss a')}`
		});
	}
}

PushNotificationIOS.addEventListener('localNotification', (notification) => {
  if (AppState.currentState !== 'background') {
		Alert.alert('', notification.getMessage());
  }
});

const notifications = {
	notifyAddress5minutely
};

export default notifications;