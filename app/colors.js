const colors = {
  accent: '#172b4c',
  secondary: '#647289',
  selectedColor: '#EEEEEE'
};

export default colors;