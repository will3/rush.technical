import _ from 'lodash';

const settings = {
	gmKey: 'AIzaSyDOjtqd_ytJAeGcoIoGOGhjqLL3CoT-8-0',
	showErrorVerbose: true
};

if (!__DEV__) {
	_.extend(settings, {
		showErrorVerbose: false,
		gmKey: '<production key>'
	});
}

export default settings;