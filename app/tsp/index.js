import api from '../api';
import makeTsp from './tspsolver';

function solveRoundTrip(params, resultCallback, progressCallback, errorCallback) {
	const tsp = makeTsp(api);
	const places = params.places;
	const currentLocation = params.currentLocation;

	tsp.addWaypoint(currentLocation);

	for (var i = 0; i < places.length; i++) {
		const waypoint = {
			longitude: places[i].geometry.location.lng,
			latitude: places[i].geometry.location.lat
		};
		tsp.addWaypoint(waypoint);
	}

	tsp.solveRoundTrip(function() {
		resultCallback(tsp);
	}, function(data) {
		progressCallback(data);
	}, errorCallback);
}

export default solveRoundTrip;